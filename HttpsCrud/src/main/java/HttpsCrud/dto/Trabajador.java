package HttpsCrud.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Trabajadores")

public class Trabajador {

	
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private Long id;
		@Column(name = "nombre")
		private String nombre;
		@Column(name = "trabajo")
		private String trabajo;
		@Column (name = "salario")
		private int salario;
		
		

	
	public Trabajador() {
		
	}
	

	 public Trabajador(Long id,String nombre,String trabajo) {
	this.id=id;
	this.nombre= nombre;
	this.trabajo = trabajo;
	this.salario = 0;

		
	}
	
	public Trabajador(Long id,String nombre,String trabajo,int salario) {
	this.id=id;
	this.nombre= nombre;
	this.trabajo = trabajo;
	this.salario = salario;

	
	
		
		
	}




	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public  String getTrabajo() {
		return trabajo;
	}



	public void setTrabajo(String trabajo) {
		this.trabajo = trabajo;
	}
	
	
	
	
	public int getSalario() {
	
		return salario;
		
		
		
		
	}



	public void setSalario(int salario) {
		this.salario = salario;
	}



	@Override
	
	public String toString() {
		return "Trabajador [id=" + id + ", nombre=" + nombre + ", trabajo=" + trabajo + "]";
	}
	
	
	
	
	
	public static int asignarSalario(String trabajo) {
		int salario = 0;
		
		if(trabajo.contentEquals("conserje")) {
			salario = 3000;
		}else if(trabajo.contentEquals("director")) {
			salario = 10000;
		}else if(trabajo.contentEquals("programador")) {
			salario = 50000;
		}else if(trabajo.contentEquals("administrativo")) {
			salario = 60000;
		}
		
		return salario;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	}
	
	
	
	
	
	
	
	
	
	

