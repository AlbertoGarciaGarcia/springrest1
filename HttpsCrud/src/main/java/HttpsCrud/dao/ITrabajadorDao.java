package HttpsCrud.dao;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import HttpsCrud.dto.Trabajador;
public interface ITrabajadorDao extends JpaRepository<Trabajador,Long> {

	public List<Trabajador> findByTrabajo(String trabajo);

}
