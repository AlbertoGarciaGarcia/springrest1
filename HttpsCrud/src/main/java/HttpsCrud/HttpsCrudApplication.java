package HttpsCrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HttpsCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(HttpsCrudApplication.class, args);
	}

}
