package HttpsCrud.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import HttpsCrud.dto.Trabajador;
import HttpsCrud.service.TrabajadorServiceImpl;

@RestController
@RequestMapping("/api")
public class TrabajadorController {

	@Autowired
	TrabajadorServiceImpl trabajadorServiceImpl;

	@GetMapping("/trabajadores")
	public List<Trabajador> listarTrabajadores(){
		return trabajadorServiceImpl.listarTrabajadores();
	}
	@GetMapping("/trabajadores/trabajador/{trabajo}")
	public List<Trabajador> buscarTrabajo(@PathVariable(name="trabajo")String trabajo){
		return trabajadorServiceImpl.buscarbyTrabajo(trabajo);
	}
	
	@PostMapping("/trabajadores")
	public Trabajador guardarTrabajador(@RequestBody Trabajador trabajador) {
		return trabajadorServiceImpl.guardarTrabajador(trabajador);
	}
	
	
	@GetMapping("/trabajadores/{id}")
	public Trabajador trabajadorPorId(@PathVariable(name="id")Long id) {
		
		Trabajador trabajadorporId = new Trabajador();
		
		trabajadorporId=trabajadorServiceImpl.trabajadorPorId(id);
		
		System.out.println("Trabajador por ID:" + trabajadorporId);
		
		return trabajadorporId;
	}
	

	
	
	
	@PutMapping("/trabajadores/{id}")
	public Trabajador actualizarTrabajador(@PathVariable(name="id")Long id,@RequestBody Trabajador Trabajador) {
		
		Trabajador trabajador= new Trabajador();
		Trabajador trabajadoractualizado= new Trabajador();
		
		trabajador= trabajadorServiceImpl.trabajadorPorId(id);
		trabajadoractualizado.setId(Trabajador.getId());
		trabajadoractualizado.setNombre(Trabajador.getNombre());
		trabajadoractualizado.setTrabajo(Trabajador.getTrabajo());
		trabajadoractualizado.setSalario(HttpsCrud.dto.Trabajador.asignarSalario(Trabajador.getTrabajo()));

		
		trabajadoractualizado = trabajadorServiceImpl.actualizarTrabajador(trabajadoractualizado);
		
		System.out.println("El Trabajador actualizado es: "+ trabajadoractualizado);
		
		return trabajadoractualizado;
	}
	
	@DeleteMapping("/trabajadores/{id}")
	public void eliminarTrabajador(@PathVariable(name="id")Long id) {
		trabajadorServiceImpl.eliminarTrabajador(id);
	}
	
	
	
	
}
