package HttpsCrud.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import HttpsCrud.dao.ITrabajadorDao;
import HttpsCrud.dto.Trabajador;
@Service
public class TrabajadorServiceImpl implements ITrabajadorService{

	@Autowired
	 ITrabajadorDao  TrabajadorDao;
	
	
	@Override
	public List<Trabajador> listarTrabajadores() {

		return TrabajadorDao.findAll();
	}
	
	@Override
	public List<Trabajador> buscarbyTrabajo(String trabajo) {

		return TrabajadorDao.findByTrabajo(trabajo);
	}
	

	@Override
	public Trabajador guardarTrabajador(Trabajador trabajador) {
		trabajador.setSalario(HttpsCrud.dto.Trabajador.asignarSalario(trabajador.getTrabajo()));
		return TrabajadorDao.save(trabajador);
	}

	@Override
	public Trabajador trabajadorPorId(Long id) {
		// TODO Auto-generated method stub
		return TrabajadorDao.findById(id).get();
	}
	

	
	
	
	
	
	
	

	@Override
	public Trabajador actualizarTrabajador(Trabajador trabajador) {
		// TODO Auto-generated method stub
		return TrabajadorDao.save(trabajador);
	}

	@Override
	public void eliminarTrabajador(Long id) {
		TrabajadorDao.deleteById(id);
		
	}

	@Override
	public List<Trabajador> buscarTrabajo() {
		// TODO Auto-generated method stub
		return null;
	}

	

	
	
	
	
	
	
	
	
	
	
}
