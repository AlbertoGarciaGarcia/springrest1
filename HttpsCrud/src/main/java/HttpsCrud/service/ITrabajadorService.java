package HttpsCrud.service;
import java.util.List;

import HttpsCrud.dto.Trabajador;
public interface ITrabajadorService {
	public List<Trabajador> listarTrabajadores();
	
	public List<Trabajador> buscarTrabajo();
	
	
	public Trabajador guardarTrabajador(Trabajador trabajador);
	
	public Trabajador trabajadorPorId(Long id);
	
	public Trabajador actualizarTrabajador(Trabajador trabajador);
	
	public void eliminarTrabajador(Long id);

	public List<Trabajador> buscarbyTrabajo(String trabajo);
	
	
}
